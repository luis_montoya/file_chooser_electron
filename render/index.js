'use strict'

const { ipcRenderer } = require('electron')
const axios =require('axios')

let projectDirectoryPath_

// 
document.getElementById('projectSelectionForm').addEventListener('submit', (evt) => {
  // prevent default refresh functionality of forms
  evt.preventDefault()

  // input on the form
  const input = evt.target[0]

  // send todo to main process
  // ipcRenderer.send('add-todo', input.value)
  console.log(input.value)
  // reset input
  input.value = ''
})

document.querySelector('#dirs').addEventListener('click', (evt) => {
  ipcRenderer.send('directory-choser')
})

document.querySelector('#createReportBtn').addEventListener('click', (evt) => {
  console.log(projectDirectoryPath_)
  axios
  .post('http://localhost:5000/bm/api/v1.0/create_report', {
    p_path: projectDirectoryPath_
  })
  .then(res => {
    console.log(`statusCode: ${res.statusCode}`)
    console.log(res)
  })
  .catch(error => {
    console.error(error)
  })
})

ipcRenderer.on('projectPath', (event, path) => {
  const dirs = document.getElementById('dirs')
  dirs.value = path
  projectDirectoryPath_ = path
})