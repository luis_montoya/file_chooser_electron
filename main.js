'use strict'

const path = require('path')
const { app, ipcMain, dialog } = require('electron')

const Window = require('./Window')
const DataStore = require('./DataStore')

require('electron-reload')(__dirname)

function main () {
  // todo list window
  let mainWindow = new Window({
    file: path.join('render', 'index.html'),
    width: 900
  })

  // project path name 
  let projectPath

  // open directory chosser dialog
  ipcMain.on('directory-choser', (event) => {
    dialog.showOpenDialog({
      properties: ['openDirectory'] 
    }).then(result => {
      // console.log(result.canceled)
      if ( !result.canceled ) {
        // console.log(result.filePaths)
        projectPath = result.filePaths[0]
        // console.log(projectPath)
        mainWindow.send('projectPath', projectPath)
      } 
    }).catch(err => {
      console.log(err)
    })
  })

}

app.on('ready', main)

app.on('window-all-closed', function () {
  app.quit()
})
